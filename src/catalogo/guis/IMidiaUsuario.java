package catalogo.guis;

public interface IMidiaUsuario {

	public abstract void cadastro();

	public abstract void exclusao();

	public abstract void consulta();

	public abstract void exibirDadosMidia();

	public abstract void editar();

	public abstract void salvar();

	public abstract void carregar();

}
